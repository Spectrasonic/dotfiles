#!/bin/sh


# Verifica si el directorio Minecraft existe

if [ ! -d "minecraft" ]; then
    mkdir minecraft
fi

cd minecraft

wget https://launcher.mojang.com/download/Minecraft.tar.gz

tar -zxvf Minecraft.tar.gz --strip-components 1

sudo mv minecraft-launcher /bin/

# ln -rs /bin/minecraft-launcher ~/Escritorio/minecraft-launcher

rm -rfv Minecraft.tar.gz

cd ..
rm -rfv minecraft

# descarga, renombra y mueve el icono al directorio de minecraft


echo "#\!/usr/bin/env xdg-open\

[Desktop Entry]\

Version=1.0\

Name=Minecraft\

Comment=Minecraft Game\

Exec=/bin/minecraft-launcher\

Icon=~/.minecraft/icon.png\

Terminal=false\

Type=Application\

Categories=Game;\

StartupNotify=true\

Name[en_US]=Minecraft\

Name[es_ES]=Minecraft\

Name[es_MX]=Minecraft" > minecraft.desktop

# mv minecraft.desktop /usr/share/applications
# ln -rs /usr/share/applications/minecraft.desktop ~/Escritorio

# if [ ! -f "icon.png" ]; then
# curl -O icon.png https://www.minecraft.net/etc.clientlibs/minecraft/clientlibs/main/resources/img/GrassBlock_HighRes.png
# # mv GrassBlock_HighRes.png icon.png
# mv icon.png ~/.minecraft
# fi







echo
echo
echo "$(tput setaf 2)Minecraft Instalado!"
