#!/bin/sh


#Script to reinstall all ZSH, OhMyZSH and Powerlevel10K theme

# Instalar zshrc
# sudo apt install -y zsh

# remover version anterior de Oh-My-ZSH
rm -rf ~/.oh-my-zsh

# renombrar .zshrc para evitar comflictos
mv ~/.zshrc ~/.zshrc-bak

# Reinstalar oh my zsh
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"


# Install Powelevel10K Theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Renombrar zshrc
mv ~/.zshrc-bak ~/.zshrc


#Reintalar Plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

#Reload zsh
source .zshrc

echo "$(tput setaf 11)Oh-My-ZSH Reinstalado"
echo ''
echo "$(tput setaf 2)Done!"

#echo "$(tput setaf 1)Red text $(tput setab 7)and white background$(tput sgr 0)