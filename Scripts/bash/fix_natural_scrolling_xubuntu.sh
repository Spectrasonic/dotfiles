#!/bin/bash

# This Script Fixes the Natural Scrolling in Xubuntu
synclient VertScrollDelta=-235
synclient HorizScrollDelta=-235
