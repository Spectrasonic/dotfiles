#!/bin/bash

# Version Variable
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')
# Downlaod .tar.gz from Github
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
# Extrct file
sudo tar xf lazygit.tar.gz -C /usr/local/bin lazygit
# Remove .tar.gz
rm -rf lazygit.tar.gz
echo
echo "$(tput bold)$(tput setaf 2)Lazy Git $(tput setaf 4)${LAZYGIT_VERSION} $(tput setaf 2)installed successfully! $(tput sgr 0)"