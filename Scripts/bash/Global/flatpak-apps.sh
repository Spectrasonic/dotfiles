flatpak install flathub -y \
  com.spotify.Client \
  com.github.jeromerobert.pdfarranger \
  com.sublimetext.three \
  org.filezillaproject.Filezilla \
  com.github.tchx84.Flatseal \
  net.blockbench.Blockbench \
  org.onlyoffice.desktopeditors \
  com.authy.Authy \
  cc.arduino.IDE2 \
  com.github.IsmaelMartinez.teams_for_linux \
  io.github.shiftey.Desktop
# org.qbittorrent.qBittorrent \
# com.discordapp.Discord \
# org.fritzing.Fritzing \
# com.system76.Popsicle \
# md.obsidian.Obsidian \
# org.thonny.Thonny \
# com.microsoft.Teams \
# org.nomacs.ImageLounge \
# dev.lapce.lapce \
# com.github.marktext.marktext \
# io.github.kotatogram \
# com.raggesilver.BlackBox \
# org.kde.dolphin io.mpv.Mpv \
