#!/bin/bash
# download Firefox Dev edition script
# Selector Menú

echo "$(tput bold)$(tput setaf 4)Select Firefox dev Edition Language"
echo
echo "$(tput setaf 4).............."
echo "1) Mexican Spanish" 
echo "2) EU English" 
echo "$(tput setaf 4)..............$(tput sgr0)"
echo

read n
case $n in
    "1")
        echo "Downloading Firefox Dev Edition Spanish"
        wget -O firefox-dev.tar.bz2 'https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=es-MX'
        ;;
    "2") 
        echo "Downloading Firefox Dev Edition English"
        wget -O firefox-dev.tar.bz2 'https://download.mozilla.org/?product=firefox-devedition-latest-ssl&os=linux64&lang=en-US'
        ;;
    *)
        echo "$(tput setaf 1)Invalid option, Run again the Script$(tput sgr0)"
        exit 0 
esac

if [ ! -d  $HOME/opt ]; then
    mkdir  $HOME/opt
    echo "$(tput bold)$(tput setaf 2)Created ~/opt directory$(tput sgr0)"
else
    echo "$(tput bold)$(tput setaf 2)Found Directory$(tput sgr0)"
fi

tar -xf firefox-dev.tar.bz2 -C $HOME/opt
rm -rf firefox-dev.tar.bz2

cd $HOME/opt/

if [ -d "$HOME/opt/firefox" ]; then
    mv firefox firefox-dev
fi

cd $HOME/opt/firefox-dev
sudo ln -rsf firefox /usr/local/bin

echo "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Firefox Developer Edition
Comment=Firefox Dev edition
Exec=/usr/local/bin/firefox
Icon=$HOME/opt/firefox-dev/browser/chrome/icons/default/default128.png
Terminal=false
Categories=Network;" > firefox-dev.desktop

chmod +x firefox-dev.desktop

ln -rsf $PWD/firefox-dev.desktop $HOME/.local/share/applications/

echo
echo "$(tput bold)$(tput setaf 2)Descargado $(tput setaf 4)Firefox Developer Edition$(tput sgr0)"