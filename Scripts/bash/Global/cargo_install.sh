#!/bin/bash

# Install cargo from source

if [ ! -d "$HOME/.cargo" ]; then
    echo "Cargo not installed"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    exit 1
else
    echo
    echo "$(tput bold)$(tput setaf 2)Cargo already installed$(tput sgr 0)"
    echo "$(tput bold)$(tput setaf 3)Now installing cargo packages$(tput sgr 0)"
    echo "$(tput bold)$(tput setaf 6)Use $(tput setaf 4)cargo install <package> $(tput setaf 6)To install some Package$(tput sgr 0)"
    exit 0
fi
