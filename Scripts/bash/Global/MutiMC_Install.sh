#!/bin/bash

# Downlaod .tar.gz from Github
curl -Lo MultiMC.tar.gz "https://files.multimc.org/downloads/mmc-stable-lin64.tar.gz"

# Extrct file
if [ ! -d "$HOME/opt/MultiMC" ]; then
	mkdir -p $HOME/opt/MultiMC
	echo "$(tput setaf 1)Directory Created$(tput sgr0)"
else
	echo "$(tput setaf 2)Directory already exist, Extracting...$(tput sgr0)"
fi

sudo tar xf multimc.tar.gz -C $HOME/opt/MultiMC --strip-components 1
sudo chmod +x $HOME/opt/MultiMC/
sudo ln -rsf $HOME/opt/MultiMC/MultiMC/ /usr/local/bin/

# Remove .tar.gz
rm -rf MultiMC.tar.gz

#Download Icon
sudo curl -Lo $HOME/opt/MultiMC/icon.png "https://avatars2.githubusercontent.com/u/5411890"

# Create Desktop Entry
printf "%s" "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=MultiMC
Comment=A custom launcher for Minecraft that allows you to easily manage multiple installations of Minecraft at once.
Exec=MultiMC
Icon=/home/spectra/opt/MultiMC/icon.png
Terminal=false
Categories=Game;ActionGame;AdventureGame;Simulation;
Keywords=game;minecraft;launcher;mc;multimc;polymc;" > $HOME/opt/MultiMC/MultiMC.desktop

chmod +x $HOME/opt/MultiMC/MultiMC.desktop

ln -rsf $HOME/opt/MultiMC/MultiMC.desktomup $HOME/.local/share/applications/

# Finish
echo
echo "$(tput bold)$(tput setaf 2)MultiMC Installed Successfully! $(tput sgr 0)"