#!/bin/sh

# Verifica si el directorio Minecraft existe en la carpeta OPT del usuario 

cd ~/opt

if [ ! -d "$HOME/opt/minecraft" ]; then
    mkdir minecraft
    echo "Created Minecraft Directory"
else
    echo "Directory Found"
fi
cd $HOME/opt/minecraft
wget -q https://launcher.mojang.com/download/Minecraft.tar.gz
tar -zxvf Minecraft.tar.gz --strip-components 1
rm -rfv Minecraft.tar.gz

$HOME
# Crea el archivo Entrade de Menu

if [ ! -f "$HOME/opt/minecraft/minecraft.desktop" ]; then

echo "#\!/usr/bin/env xdg-open\

[Desktop Entry]\

Version=1.0\

Name=Minecraft Official Launcher\

Comment=Minecraft Game\

Exec=$HOME/opt/minecraft/minecraft-launcher\

Icon=$HOME/opt/minecraft/icon.png\

Terminal=false\

Type=Application\

Categories=Game;\

StartupNotify=true\

Name[en_US]=Minecraft\

Name[es_ES]=Minecraft\

Name[es_MX]=Minecraft" > minecraft.desktop
cp -f minecraft.desktop ~/.local/share/applications/

else
 echo "$(tput bold)$(tput setaf 2)Minecraft Directory founded!$(tput sgr0)"
fi

# Set Execute permissions to files

chmod +x minecraft-launcher
chmod +x minecraft.desktop

# ln -rs /usr/share/applications/minecraft.desktop ~/Escritorio


# Download Icon

if [ ! -f "icon.png" ]; then
    wget -O icon.png 'https://i.imgur.com/gUujan9.png'
fi

# clear
echo
echo "$(tput bold)$(tput setaf 2)Minecraft Instalado! $(tput sgr0)"