#!/bin/bash

# Verify if the directory exists
if [ ! -d $HOME/opt ]; then
    mkdir -p ~/opt
    echo "$(tput bold)$(tput setaf 2)Created ~/opt directory$(tput sgr0)"
else
    echo "$(tput bold)$(tput setaf 2)Found Directory$(tput sgr0)"
fi

# Download Discord with custom name
wget -O Discord.tar.gz "https://discord.com/api/download?platform=linux&format=tar.gz"

# Unpack Discord and remove tar file

tar -xvf Discord.tar.gz -C $HOME/opt
rm -rf Discord.tar.gz
echo "[Desktop Entry]
Name=Discord
StartupWMClass=discord
Comment=All-in-one voice and text chat for gamers that's free, secure, and works on both your desktop and phone.
GenericName=Internet Messenger
Exec=$HOME/opt/Discord/Discord
Icon=$HOME/opt/Discord/discord.png
Type=Application
Categories=Network;InstantMessaging;
Path=/usr/bin" > $HOME/opt/Discord/discord.desktop

chmod +x $HOME/opt/Discord/discord.desktop
ln -rsf $HOME/opt/Discord/discord.desktop $HOME/.local/share/applications

# clear
echo
echo "$(tput bold)$(tput setaf 2)Installed Discord.$(tput sgr0)"