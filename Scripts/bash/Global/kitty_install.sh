#!/bin/bash

# Kitty Var
KITTY_VERSION=$(curl -s "https://api.github.com/repos/kovidgoyal/kitty/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

# Create Directory
if [ ! -d "$HOME/opt/Kitty" ]; then
	mkdir -p $HOME/opt/Kitty
	echo "$(tput setaf 1)Directory Created$(tput sgr0)"
else
	echo "$(tput setaf 2)Directory already exist, Extracting...$(tput sgr0)"
fi

# Download Binary
curl -Lo kitty.txz "https://github.com/kovidgoyal/kitty/releases/download/v${KITTY_VERSION}/kitty-${KITTY_VERSION}-x86_64.txz"

# Extract and Remove
tar xf kitty.txz -C $HOME/opt/Kitty
rm kitty*.txz
# Set Execute Permision to Directory and create a link to binary in /bin
chmod +x $HOME/opt/Kitty/
sudo ln -rsf $HOME/opt/Kitty/bin/kitty /usr/local/bin/

# Create Desktop Entry
printf "%" "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Kitty Terminal
Comment=Kitty Terminal Emulator
Exec=kitty
Icon=$HOME/opt/Kitty/share/icons/hicolor/256x256/apps/kitty.png
Terminal=false
Categories=Development;System;
Keywords=Terminal;terminal;kitty" > $HOME/opt/Kitty/bin/kitty.desktop

chmod +x $HOME/opt/Kitty/bin/Kitty.desktop

# Set Execute Permission and create link in Application local directory
if [ ! -d "$HOME/.local/share/applications/" ]; then
	mkdir -p $HOME/.local/share/applications/
	echo "$(tput setaf 1)Directory Created$(tput sgr0)"
	ln -rsf $HOME/opt/Kitty/bin/kitty.desktop $HOME/.local/share/applications/kitty.desktop

else
	echo "$(tput setaf 2)Directory already exist, copying file...$(tput sgr0)"
	ln -rsf $HOME/opt/Kitty/bin/kitty.desktop $HOME/.local/share/applications/kitty.desktop
fi
# Config files
echo "$(tput setaf 3)Clone config files from repo... $(tput sgr 0)"
git clone https://gitlab.com/config-files3/kitty-config.git ~/.config/kitty --depth 1
# mkdir -p $HOME/.config/kitty
# curl -Lo $HOME/.config/kitty/kitty.conf https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/kitty/kitty.conf

# Finish
echo
echo "$(tput bold)$(tput setaf 2)Kitty Terminal Installed $(tput setaf 4)$KITTY_VERSION $(tput sgr 0)"
