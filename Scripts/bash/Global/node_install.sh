#!/bin/bash

# This script will install node.js on your system. yu can select the version lts or current

node_current(){
    NODE_V=$(curl -s https://nodejs.org/en/download/current/ | grep -oP '<strong>\K[^<]+' | sed 's/<\/strong>//')

    echo "NODE VERSION: $(tput setaf 2)$NODE_V$(tput sgr 0)"

    wget https://nodejs.org/dist/v$NODE_V/node-v$NODE_V-linux-x64.tar.gz -O node.tar.gz

    sudo tar -xzf node.tar.gz -C /opt/
    rm node.tar.gz

    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/node /usr/local/bin/node
    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/npm /usr/local/bin/npm
    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/npx /usr/local/bin/npx

    echo "$(tput setaf 2)$(node -v)"
    echo "$(tput setaf 2)$(npm -v)"

    echo "$(tput setaf 2)Node $(tput setaf 2)$NODE_V $(tput setaf 2)Current Installed $(tput sgr 0)"
}

node_lts(){
    NODE_V=$(curl -s https://nodejs.org/en/download | grep -oP '<strong>\K[^<]+' | sed 's/<\/strong>//')

    echo "NODE VERSION: $(tput setaf 2)$NODE_V$(tput sgr 0)"

    wget https://nodejs.org/dist/v$NODE_V/node-v$NODE_V-linux-x64.tar.gz -O node.tar.gz

    sudo tar -xzf node.tar.gz -C /opt/
    rm node.tar.gz

    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/node /usr/local/bin/node
    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/npm /usr/local/bin/npm
    sudo ln -rsf /opt/node-v$NODE_V-linux-x64/bin/npx /usr/local/bin/npx

    echo "$(tput setaf 2)$(node -v)"
    echo "$(tput setaf 2)$(npm -v)"

    echo "$(tput setaf 2)Node $(tput setaf 2)$NODE_V $(tput setaf 2)LTS Installed $(tput sgr 0)"
}

main(){
    echo "Select a version of node.js to install"
    echo "1. LTS"
    echo "2. Current"
    echo "Q. Exit"

    read choose
    case $choose in
    1)
        node_lts

    ;;
    2)
        node_current
    ;;
    "Q"|"q")
        command clear
        exit 0
    ;;
    *)
        echo "Invalid option"
        command clear
        main
    ;;
    esac
}

main

