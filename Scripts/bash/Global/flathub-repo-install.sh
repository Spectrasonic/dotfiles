#!/bin/sh
# Install flathub repo

flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
echo "$(tput bold)$(tput setaf 2)Flathub Repo Instalado $(tput sgr0)"
echo "$(tput bold)$(tput setaf 4)Reinicie el sistema para aplicar los cambios. $(tput sgr0)"

