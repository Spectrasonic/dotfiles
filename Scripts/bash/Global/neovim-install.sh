#!/bin/bash

# Install Neovim from Github Repository

LAST_VERSION=$(curl -s "https://api.github.com/repos/neovim/neovim/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

echo "$(tput setaf 3)Installing Neovim: $(tput setaf 4)$LAST_VERSION $(tput sgr0)"

wget "https://github.com/neovim/neovim/releases/download/${LAST_VERSION}/nvim-linux64.tar.gz"

sudo tar -xf nvim-linux64.tar.gz -C /opt/
sudo ln -rsf /opt/nvim-linux64/bin/nvim /usr/local/bin
rm -rf nvim-linux64.tar.gz

# config files
echo "$(tput setaf 3)Clone config files from repo... $(tput sgr 0)"
git clone https://github.com/spectrasonic117/NvChad.git ~/.config/nvim --depth 1

# mkdir -p $HOME/.config/nvim/
# curl -Lo $HOME/.config/nvim/nvim.tar.gz "https://www.dropbox.com/s/ye1yw10ir81yyv8/nvim.tar.gz?dl=1"
# sudo tar -xf $HOME/.config/nvim/nvim.tar.gz -C $HOME/.config/nvim/
# rm -rf $HOME/.config/nvim/nvim.tar.gz
# curl -Lo $HOME/.vimrc https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/neovim/.vimrc
# curl -Lo $HOME/.vimrc "https://www.dropbox.com/s/q6htuysl8avuj9p/.vimrc?dl=1"

echo
echo "$(tput bold)$(tput setaf 2)Installed Neovim with NvChad $(tput setaf 4)$LAST_VERSION$(tput setaf 2).$(tput sgr0)"