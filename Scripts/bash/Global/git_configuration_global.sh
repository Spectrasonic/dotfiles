#!/bin/bash

# Git configuration
git config --global user.name "Manuel Heredia"
git config --global user.email "Spectrasonic117@gmail.com"

git config --global init.defaultBranch master

git config --global core.editor "/usr/local/bin/nvim"

git config --global core.excludesfile ~/.gitignore_global

# Git Aliasses Commands

# template
# git config --global alias.<aliascommand> <git command>

# Stage Area
git config --global alias.all add .
git config --global alias.a add -A

# Switch / Branches
git config --global alias.sw switch
git config --global alias.sm switch master
git config --global alias.sd switch dev

# Commit
git config --global alias.comm commit
git config --global alias.empty commit --allow-empty "Empty Commit"

# Push
git config --global alias.pom push -u origin master
git config --global alias.pod push -u origin dev

# Log
git config --global alias.lg log --oneline --graph --decorate