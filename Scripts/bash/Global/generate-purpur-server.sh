#!/bin/sh

# Este scrpt crea un servidor de Minecraft con la ultima build de PurpurMC


# echo Escribe Versión Version...
# read version


# Variables
MC_VERSION=1.18.2
PURPUR_URI="https://api.purpurmc.org/v2/purpur/${MC_VERSION}/latest/download"
INIT_RAM="1G"
MAX_RAM="2G"
# EXTRA_FLAGS=""


# If directory debug doesn't exist, create one
if [ ! -d "server" ]; then
    echo "$(tput setaf 3)$(mkdir server)"   
fi

echo
echo
echo "$(tput setaf 2)Carpeta Server Descargado."
echo
echo
cd server

# Download a purpur server file
if [ ! -f "purpur.jar" ]; then
    echo "$(tput setaf 3)$(wget -O purpur.jar "${PURPUR_URI}")"

fi
echo
echo
echo "$(tput setaf 2)Server Descargado."
echo
echo
# Create Eula File
echo "eula=true" > eula.txt
echo
echo
echo "$(tput setaf 2)Archivo Eula creado."

echo
echo
# Create the icon server
echo "$(tput setaf 3)$(curl -O https://bitbucket.org/Spectrasonic/svg-rep/raw/beb78d67ad4f636cc8c68d421bc54ba9a784aeb4/PNG%20Images/server-icon.png)"
echo
echo
echo "$(tput setaf 2)Icono Servidor creado."
echo
echo
# Crete a Start.sh File
echo "#!/bin/sh \

java -jar -Xms${INIT_RAM} -Xmx${MAX_RAM} purpur.jar nogui" > start.sh
chmod 777 start.sh
chmod 777 purpur.jar

echo
echo
echo
echo "$(tput setaf 2)Servidor creado correctamente."


