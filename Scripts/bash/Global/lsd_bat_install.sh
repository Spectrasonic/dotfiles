#!/bin/bash

# Fetch latest Version
BAT_VERSION=$(curl -s "https://api.github.com/repos/sharkdp/bat/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')
LSD_VERSION=$(curl -s "https://api.github.com/repos/lsd-rs/lsd/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

# Print versions to be installed
echo "$(tput setaf 3)BAT version: $(tput setaf 5)$BAT_VERSION$(tput sgr 0)"
echo "$(tput setaf 3)LSD version: $(tput setaf 5)$LSD_VERSION$(tput sgr 0)"

# Download Binaries
curl -Lo bat.tar.gz "https://github.com/sharkdp/bat/releases/download/v$BAT_VERSION/bat-v$BAT_VERSION-x86_64-unknown-linux-gnu.tar.gz"
curl -Lo lsd.tar.gz "https://github.com/lsd-rs/lsd/releases/download/v$LSD_VERSION/lsd-v$LSD_VERSION-x86_64-unknown-linux-gnu.tar.gz"

# Extract Files
tar -xvf bat.tar.gz
tar -xvf lsd.tar.gz

# Move Files
sudo mv bat-v$BAT_VERSION-x86_64-unknown-linux-gnu/bat /usr/local/bin
sudo mv lsd-v$LSD_VERSION-x86_64-unknown-linux-gnu/lsd /usr/local/bin

# Remove tar.gz files
rm -rf bat.tar.gz
rm -rf lsd.tar.gz

# Remove extracted folders
rm -rf bat-v$BAT_VERSION-x86_64-unknown-linux-gnu
rm -rf lsd-$LSD_VERSION-x86_64-unknown-linux-gnu

echo "$(tput bold)Succesfully installed Bat: $(tput setaf 5)${BAT_VERSION}$(tput sgr 0) and LSD: $(tput setaf 5)${LSD_VERSION} $(tput sgr 0)"
