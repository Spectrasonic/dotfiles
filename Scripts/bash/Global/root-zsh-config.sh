#!/bin/sh

#Set the same ZSH configuration for Root

ln -rsf /home/spectra/.zshrc /root
ln -rsf /home/spectra/.p10k.zsh /root
ln -rsf /home/spectra/.oh-my-zsh /root
