#!/bin/sh

# This script will install the Prism Launcher and all of its dependencies.

# Variable
PRISM_VERSION=$(curl -s "https://api.github.com/repos/PrismLauncher/PrismLauncher/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

# Download .tar.gz from Github
curl -Lo PrismLauncher.tar.gz "https://github.com/PrismLauncher/PrismLauncher/releases/download/$PRISM_VERSION/PrismLauncher-Linux-$PRISM_VERSION.tar.gz"


# Extrct file in folder
if [ ! -d "$HOME/opt/Prism\ Launcher" ]; then
	mkdir -p $HOME/opt/Prism\ Launcher
	echo "$(tput setaf 1)Directory Created$(tput sgr0)"
else
	echo "$(tput setaf 2)Directory already exist, Extracting...$(tput sgr0)"
fi

tar xf PrismLauncher.tar.gz -C $HOME/opt/Prism\ Launcher --strip-components 0
chmod +x $HOME/opt/Prism\ Launcher
sudo ln -rsf $HOME/opt/Prism\ Launcher/bin/prismlauncher /usr/local/bin/

rm -rf PrismLauncher*.tar.gz

# create desktop file entry
echo "[Desktop Entry]
Version=1.0
Name=Prism Launcher
Comment=A custom launcher for Minecraft that allows you to easily manage multiple installations of Minecraft at once.
Type=Application
Terminal=false
Exec=/usr/local/bin/prismlauncher
StartupNotify=true
Icon=$HOME/opt/Prism Launcher/share/icons/hicolor/scalable/apps/org.prismlauncher.PrismLauncher.svg
Categories=Game;ActionGame;AdventureGame;Simulation;
Keywords=game;minecraft;launcher;mc;multimc;polymc;
StartupWMClass=PrismLauncher
MimeType=application/zip;application/x-modrinth-modpack+zip" > $HOME/opt/Prism\ Launcher/prismlauncher.desktop

# File permission
chmod +x $HOME/opt/Prism\ Launcher/prismlauncher.desktop
ln -rsf $HOME/opt/Prism\ Launcher/prismlauncher.desktop $HOME/.local/share/applications/

# Finish
echo "$(tput bold)$(tput setaf 2)Prism Launcher $(tput setaf 4)$PRISM_VERSION $(tput setaf 2)installed successfully! $(tput sgr 0)"