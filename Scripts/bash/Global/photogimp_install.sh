#!/bin/bash

# Script to install Photogimp Flatpak Linux 

if [ -d /etc/apt/ ]; then
    echo "$(tput setaf 4)Installer for $(tput setaf 3)APT $(tput sgr 0)"
    sudo apt install -y zip unzip
elif [ -d /etc/dnf/ ]; then
    echo "$(tput setaf 4)Installer for $(tput setaf 3)DNF $(tput sgr 0)"
    sudo yum install -y zip unzip
else
    echo "$(tput setaf 1)Your distro is not supported by this script. $(tput sgr 0)"
    exit 1
fi

echo "$(tput setaf 3)Downloading files... $(tput sgr 0)"
PG=$(curl -s "https://api.github.com/repos/Diolinux/PhotoGIMP/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')
wget -S https://github.com/Diolinux/PhotoGIMP/releases/download/$PG/PhotoGIMP.zip -O PhotoGIMP.zip

echo "$(tput setaf 3)Extracting files... $(tput sgr 0)"
unzip PhotoGIMP.zip

# Copying app configuration
echo "$(tput setaf 3)Copying app Configuration... $(tput sgr 0)"
if [ ! -d $HOME/.var/app/org.gimp.GIMP ]; then
    echo "$(tput setaf 1) Dir not found, Creating... $(tput sgr 0)"
    mkdir -p $HOME/.var/app/org.gimp.GIMP
fi
cp -r $PWD/PhotoGIMP-master/.var/app/org.gimp.GIMP $HOME/.var/app/org.gimp.GIMP

# Copyng icons
echo "$(tput setaf 3)Copying app Icons... $(tput sgr 0)"
cp -r $PWD/PhotoGIMP-master/.local/share/applications/org.gimp.GIMP.desktop $HOME/.local/share/applications/org.gimp.GIMP.desktop
cp -r $PWD/PhotoGIMP-master/.local/share/icons/hicolor $HOME/.local/share/icons/hicolor

# Remove Unnecesary files
rm -rfv PhotoGIMP.zip
rm -rf $PWD/PhotoGIMP-master/

echo "$(tput setaf 2)Installed PhotoGIMP $(tput sgr 0)"
exit 0
