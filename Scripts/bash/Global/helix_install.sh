#!/bin/sh

# Sciprt to install Helix from Github Repository

if [ ! -d /opt/helix ]; then
    sudo mkdir -p /opt/helix
fi

HELIX_VERSION=$(curl -s "https://api.github.com/repos/helix-editor/helix/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

wget -O helix.tar.gz "https://github.com/helix-editor/helix/releases/download/${HELIX_VERSION}/helix-${HELIX_VERSION}-x86_64-linux.tar.xz"


sudo tar -xf helix.tar.gz -C /opt/helix/ --strip-components=1
rm -rf helix.tar.gz
sudo chmod +x /opt/helix/hx
sudo ln -rsf /opt/helix/hx /usr/local/bin/

echo "$(tput bold)$(tput setaf 2)Installed Helix $(tput setaf 4)${HELIX_VERSION}$(tput setaf 2).$(tput sgr0)"