#!/bin/sh

TABBY_VERSION=$(curl -s "https://api.github.com/repos/Eugeny/tabby/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

if [ ! -d "$HOME/opt/Tabby" ]; then
    mkdir -p $HOME/opt/Tabby
    echo "$(tput setaf 1)Create Directory $(tput sgr 0)"
else
    echo "$(tput setaf 2)The directory Exist... Extracting $(tput sgr 0)"
fi

curl -Lo tabby.tar.gz "https://github.com/Eugeny/tabby/releases/download/v${TABBY_VERSION}/tabby-${TABBY_VERSION}-linux-x64.tar.gz"

tar -xf tabby.tar.gz -C $HOME/opt/Tabby --strip-components=1
rm tabby*.tar.gz
chmod +x $HOME/opt/Tabby
sudo ln -rsf $HOME/opt/Tabby/tabby /usr/local/bin/tabby

curl -Lo $HOME/opt/Tabby/icon.svg "https://raw.githubusercontent.com/Eugeny/tabby/master/app/assets/logo.svg"

echo "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Tabby Terminal
Comment=Next Generation Terminal
Exec=tabby
Icon=$HOME/opt/Tabby/icon.svg
Terminal=false
Categories=Development;" > $HOME/opt/Tabby/Tabby.desktop

chmod +x $HOME/opt/Tabby/Tabby.desktop
ln -rsf $HOME/opt/Tabby/Tabby.desktop $HOME/.local/share/applications

echo
echo "$(tput setaf 2)Tabby $(tput setaf 4)${TABBY_VERSION} $(tput setaf 2)Instalado $(tput sgr 0)"
