#!/bin/bash

LAST_VERSION=$(curl -s "https://api.github.com/repos/arduino/arduino-ide/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

echo ""
echo "$(tput setaf 3)Installing Arduino IDE: $(tput setaf 4)$LAST_VERSION $(tput sgr0)"
echo ""

wget "https://github.com/arduino/arduino-ide/releases/download/${LAST_VERSION}/arduino-ide_${LAST_VERSION}_Linux_64bit.zip" -O arduino-ide.zip


if [ ! -d "$HOME/opt/" ]; then
    mkdir -p $HOME/opt/
    echo "$(tput setaf 1)Directory Created$(tput sgr0)"
else
    echo "$(tput setaf 2)Directory already exist, Extracting...$(tput sgr0)"
fi

unzip -q arduino-ide.zip -d $HOME/opt/
rm -rf arduino-ide.zip
mv $HOME/opt/arduino-ide_* $HOME/opt/arduino-ide

sudo ln -rsf $HOME/opt/arduino-ide/arduino-ide /usr/local/bin/

printf "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Arduino IDE
Comment=Arduino IDE
Exec=arduino-ide
Icon=$HOME/opt/arduino-ide/resources/app/lib/e1f37bb1fd5c02b876f8..png
Terminal=false
Categories=Application
Keywords=Arduino;IDE;C++;Embedded;Electronics;Programming" > $HOME/opt/arduino-ide/Arduino-IDE.desktop

chmod +x $HOME/opt/arduino-ide/Arduino-IDE.desktop


if [ ! -d "$HOME/.local/share/applications/" ]; then
    mkdir -p $HOME/.local/share/applications/
    echo "$(tput setaf 1)Applications Directory Created$(tput sgr0)"

else
    echo "$(tput setaf 2)Applications Directory already exist, copying file...$(tput sgr0)"
fi

ln -rsf $HOME/opt/arduino-ide/Arduino-IDE.desktop $HOME/.local/share/applications/Arduino-IDE.desktop

echo "$(tput bold)$(tput setaf 2)Installed Arduino IDE $(tput setaf 4)$LAST_VERSION$(tput setaf 2).$(tput sgr0)"