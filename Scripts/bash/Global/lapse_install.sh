#!/bin/bash

# Version Variable
LAPCE_VERSION=$(curl -s "https://api.github.com/repos/lapce/lapce/releases/latest" | grep '"tag_name":' |  sed -E 's/.*"v*([^"]+)".*/\1/')

# Downlaod .tar.gz from Github
curl -Lo lapce.tar.gz "https://github.com/lapce/lapce/releases/download/v${LAPCE_VERSION}/Lapce-linux.tar.gz"

# Extrct file in folder
if [ ! -d "$HOME/opt/Lapce" ]; then
	mkdir -p $HOME/opt/Lapce
	echo "$(tput setaf 1)Directory Created$(tput sgr0)"
else
	echo "$(tput setaf 2)Directory already exist, Extracting...$(tput sgr0)"
fi

tar xf lapce.tar.gz -C $HOME/opt/Lapce --strip-components 1
chmod +x $HOME/opt/Lapce/
sudo ln -rsf $HOME/opt/Lapce/lapce /usr/local/bin/

# Remove .tar.gz
rm -rf lapce*.tar.gz

sudo curl -lo $HOME/opt/Lapce/icon.png "https://raw.githubusercontent.com/lapce/lapce/master/extra/images/logo.png"

echo "[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Lapce
Comment=Lightweight, fast, and extensible text editor written in Rust
Exec=lapce
Icon=$HOME/opt/Lapce/icon.png
Terminal=false
Categories=Development;" > $HOME/opt/Lapce/lapce.desktop

chmod +x $HOME/opt/Lapce/lapce.desktop
ln -rsf $HOME/opt/Lapce/lapce.desktop $HOME/.local/share/applications/lapse.desktop

# Finish
echo
echo "$(tput bold)$(tput setaf 2)LAPSE $(tput setaf 4)${LAPCE_VERSION} $(tput setaf 2)installed successfully! $(tput sgr 0)"