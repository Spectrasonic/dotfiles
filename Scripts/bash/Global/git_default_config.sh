#!/bin/bash

echo "$(tput setaf 4)Setting Git configuration"

#Set Global Name
git config --global user.name "Manuel Heredia"

#Set Global Email
git config --global user.email "spectrasonic117@gmail.com"

#Set Global Default Branch
git config --global init.defaultBranch master

# Set .gitignore global
git config --global core.excludesfile ~/.gitignore



# 	Set VSCODE
#git config --global core.editor "code --wait"

#   Set Neovim
git config --global core.editor "nvim"

#   Set Vim
# git config --global core.editor "vim"

# 	Set TextMATE
# git config --global core.editor "mate -w"

# 	Set Sublime Text
# git config --global core.editor "subl -n -w"


echo ''
echo ''
echo "$(tput setaf 4)Configuracion Git exitosa"