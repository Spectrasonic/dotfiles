#!/bin/bash

# Script to install Nvim from Github Repo for macOS

# Download from Github Repo
wget -O nvim.tar.gz "https://github.com/neovim/neovim/releases/download/stable/nvim-macos.tar.gz"

# Extract and install on /opt directory
sudo tar -xf nvim.tar.gz -C /opt/
rm -rf nvim.tar.gz
cd /opt/nvim-macos/bin
# Create a link file for nvim bin
sudo ln -sf $PWD/nvim /usr/local/bin
cd ~

# install NvChad

cp -r ~/.config/nvim/ ~/.config/nvim_bak/

# Install NvChad from Github Repo

git clone https://github.com/spectrasonic117/NvChad.git ~/.config/nvim --depth 1 && nvim

# Finish script
echo ""
echo "$(tput bold)$(tput setaf 2)Installed Neovim with NvChad.$(tput sgr0)"