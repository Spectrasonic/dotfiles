#!/bin/sh

# Install mcrcon for Linux or macOS Script

echo "$(tput setaf 3)Installing mcrcon for $(uname) $(tput sgr 0)"

if test "$(uname)" = "Darwin" ; then
    echo "$(tput setaf 2)mcrcon for $(tput setaf 11)macOS $(tput sgr 0)"
    curl -L -o mcrcon-macos.tar.gz "https://www.dropbox.com/s/81x60sito01kdkc/mcrcon-macos.tar.gz?dl=1"
    tar -xvf mcrcon-macos.tar.gz -C /usr/local/bin
    rm -rf mcrcon-macos.tar.gz

else
    echo "$(tput setaf 2)mcrcon for $(tput setaf 11)Linux $(tput sgr 0)"
    curl -L -o mcrcon-linux.tar.gz "https://www.dropbox.com/s/81x60sito01kdkc/mcrcon-linux.tar.gz?dl=1"
    sudo tar -xvf mcrcon-linux.tar.gz -C /usr/local/bin
    rm -rf mcrcon-linux.tar.gz

fi

echo "$(tput setaf 2)Installed mcrcon for $(tput setaf 11)$(uname) $(tput sgr 0)"
exit 1