#!/bin/bash

# Web App Manager Install via deb file

wget -O webappmanager.deb http://packages.linuxmint.com/pool/main/w/webapp-manager/webapp-manager_1.1.5_all.deb

sudo apt-get install -f ./webappmanager.deb

echo "$(tput setaf 2)Web App Manager Installed $(tput sgr 0)"
exit 1