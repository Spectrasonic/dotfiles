#!/bin/sh

sudo add-apt-repository ppa:git-core/ppa -y

sudo apt update

if [ ! -d ~/git-reps ]; then
    mkdir -p ~/git-reps
    echo "$(tput bold)$(tput setaf 2)Created ~/git-reps directory$(tput sgr0)"
else
    echo "$(tput bold)$(tput setaf 2)Found Directory$(tput sgr0)"
fi

sudo apt install -y git

clear
echo
echo "$(tput bold)$(tput setaf 2)Git Instalado$(tput sgr0)"

