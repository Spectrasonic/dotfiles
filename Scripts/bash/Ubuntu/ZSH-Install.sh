#!/bin/sh

# download and set ZSH Ubuntu

sudo apt install -y zsh

chsh -s $(which zsh)
sudo chsh -s $(which zsh)

echo "$(tput bold)$(tput setaf 2)ZSH installed and set as default shell$(tput sgr0)"