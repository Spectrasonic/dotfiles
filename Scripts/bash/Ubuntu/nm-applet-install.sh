#!/bin/sh

# Reinstall nm-applet if this was gone from Xubuntu

sudo apt-get install  -y network-manager-gnome blueman
killall nm-applet
nm-applet &

echo 'nm-applet & blueman reinstalled'
