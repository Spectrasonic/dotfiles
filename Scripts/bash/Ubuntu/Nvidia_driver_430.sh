#!/bin/sh

sudo add-apt-repository ppa:kelebek333/nvidia-legacy -y
sudo apt-get update
sudo apt install -y nvidia-340

echo
echo


# Warning: below line for kernels >= 5.11
sudo apt install -y xorg-modulepath-fix

clear

echo
echo "$(tput setaf 2)Nvidia Legacy Drivers Instalados $(tput sgr0)"
echo
echo "$(tput setaf 1)Reiniciar el sistema para aplicar cambios $(tput sgr0)"
