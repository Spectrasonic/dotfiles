#!/bin/sh

# This script is for installing ssh server on ubuntu or fedora based servers

command apt update
command apt install openssh-server ufw -y

command systemctl status ssh

command ufw allow ssh

command ip adrr show | grep inet | awk '{ print $2; }' | sed 's/\/.*$//'
