#!/bin/bash

# Install Brave Browser from oficial repos

# Deoendences
sudo apt install -y apt-transport-https curl

# Key
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | sudo tee /etc/apt/sources.list.d/brave-browser-release.list

# Install
sudo apt update
sudo apt install -y brave-browser

echo
echo "$(tput bold)$(tput setaf 2)Brave Instalado"
exit 0
