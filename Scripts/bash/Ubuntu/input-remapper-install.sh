#!/bin/sh
echo "$(tput setaf 3)Installing Dependences... $(tput sgr 0)"
sudo apt install python3-setuptools gettext git wget
echo "$(tput setaf 3)Installing Inpput Remapper... $(tput sgr 0)"
git clone https://github.com/sezanzeb/input-remapper.git
cd input-remapper && ./scripts/build.sh
sudo apt install -y ./dist/input-remapper-*.deb

echo "$(tput setaf 3)Installing config files...$(tput sgr 0)"
wget https://www.dropbox.com/s/rzkh7qpclhkflei/input-remapper-2.tar.gz?dl=1 -O input-remapper-2.tar.gz
tar -xf $PWD/input-remapper-2.tar.gz -C $HOME/.config/
rm input-remapper-2.tar.gz

echo
echo "$(tput bold)$(tput setaf 2)Input Remapper Instalado $(tput sgr0)"