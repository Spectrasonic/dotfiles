#!/bin/sh

# --- Nvidia 340 Driver --- 

sudo add-apt-repository -y ppa:kelebek333/nvidia-legacy

# --- Git ---

sudo add-apt-repository -y ppa:git-core/ppa

if [ ! -d ~/git-reps ]; then
    mkdir -p ~/git-reps
    echo "$(tput bold)$(tput setaf 2)Created ~/git-reps directory$(tput sgr0)"
else
    echo "$(tput bold)$(tput setaf 2)Found Directory$(tput sgr0)"
fi

sudo nala install -y git wget gpg

# --- Brave Browser ---

sudo nala install -y apt-transport-https curl

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

# --- Input Remaper ---

sudo nala install -y python3-setuptools gettext
git clone https://github.com/sezanzeb/input-remapper.git
cd input-remapper && ./scripts/build.sh

clear
echo
echo "$(tput bold)$(tput setaf 2)Input Remapper Instalado $(tput sgr0)"


# --- VSCode ---

wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg

sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/

sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'

rm -f packages.microsoft.gpg


# --- Install packages ---


sudo nala install -y apt-transport-https

sudo nala update && sudo nala install -y nvidia-340 xorg-modulepath-fix flatpak brave-browser ./dist/input-remapper-1.5.0.deb code zsh

# --- Flathub Reposotory ---
flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

# --- Change Shell to ZSH ---

Echo 'Escribe contraseña para cambiar la Shell...'
chsh -s $(which zsh)
sudo chsh -s $(which zsh)

# --- Install packages ---

echo
echo "$(tput bold)$(tput setaf 3)Paquetes installados(tput sgr0)"
echo ""
echo "$(tput bold)$(tput setaf 6)Nvidia 340 Driver(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)Git(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)flatpak(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)brave-browser(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)input-remapper(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)VScodium(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)ZSH and set default(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)Git(tput sgr0)"
echo "$(tput bold)$(tput setaf 6)Git(tput sgr0)"
