#!/bin/bash

# add repo

sudo echo "[notion-repackaged]
suname=notion-repackaged
baseurl=https://yum.fury.io/notion-repackaged/
enabled=1
gpgcheck=0" | tee -a /etc/yum.repos.d/notion-repackaged.repo

# Install Package
sudo dnf install notion-app

echo "$(tput bold)$(tput setaf 2)Notion App Installed $(tput sgr 0)"