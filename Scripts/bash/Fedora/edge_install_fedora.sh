#!/bin/bash

# Install Microsoft Edge on Fedora

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo dnf -y config-manager --add-repo https://packages.microsoft.com/yumrepos/edge
# Adding repo from: https://packages.microsoft.com/yumrepos/edge
sudo dnf update --refresh
sudo dnf install -y microsoft-edge-stable
echo "$(tput bold)$(tput setaf 2)Microsoft Edge Installed $(tput sgr0)"