#! /bin/bash
# enables copr repo
sudo dnf copr enable sentry/polymc
# stable releases
sudo dnf install -y polymc
# nightly releases (git master)
# sudo dnf install polymc-nightly

clear
echo
echo "$(tput bold)$(tput setaf 2)PolyMC Instalado $(tput sgr0)"
