#!/bin/bash
# Install Broadcom Driver for Fedora

# Add RPMFusion Repòsitories

#	sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#	sudo dnf update

sudo dnf install -y kmod-wl

sudo modprobe -r b43
sudo modprobe b43
sudo modprobe -r wl
sudo modprobe wl

echo "$(tput bold)$(tput setaf 2)kmod-wl Installed$(tput sgr 0)"
echo "$(tput bold)$(tput setaf 4)Reboot system to complete installation$(tput sgr 0)"