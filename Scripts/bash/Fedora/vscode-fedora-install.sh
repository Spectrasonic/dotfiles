#!/bin/bash

# Install VsCode on Fedora

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
echo "$(tput setaf 3)Repositorio creado $(tput sgr0)"
dnf check-update
sudo dnf install -y code
echo "$(tput bold)$(tput setaf 2)VsCode Instalado $(tput sgr0)"