#!/bin/bash

# Install Dependences
sudo dnf install -y python3-evdev gtksourceview4 python3-devel python3-pydantic python3-pydbus git wget

# Install the package
echo "$(tput setaf 3)Installing Inpput Remapper... $(tput sgr 0)"
sudo pip install evdev -U  # If newest version not in distros repo
sudo pip uninstall key-mapper  # In case the old package is still installed
sudo pip install --no-binary :all: git+https://github.com/sezanzeb/input-remapper.git
sudo systemctl enable input-remapper
sudo systemctl restart input-remapper

# Install the config files
echo "$(tput setaf 3)Installing config files...$(tput sgr 0)"
wget https://www.dropbox.com/s/rzkh7qpclhkflei/input-remapper-2.tar.gz?dl=1 -O input-remapper-2.tar.gz
tar -xf $PWD/input-remapper-2.tar.gz -C $HOME/.config/

rm input-remapper-2.tar.gz

echo "$(tput bold)$(tput setaf 2)Input Remapper Instalado $(tput sgr0)"