#!/bin/bash

sudo dnf install -y zsh
chsh -s $(which zsh)
sudo chsh -s $(which zsh)
echo "$(tput bold)$(tput setaf 2)Installed zsh and set it as default shell. $(tput sgr0)"
echo "$(tput bold)$(tput setaf 4)Reboot the system to apply changes. $(tput sgr0)"
