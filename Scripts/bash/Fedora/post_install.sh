#!/bin/bash

# Script Post Install Fedora


# Activate the DNF Speed
printf "%s" "
#---- DNF SpeedUp ----
max_parallel_downloads=15
defaultyes=True
keepcache=True
countme=false
" | sudo tee -a /etc/dnf/dnf.conf

# Clear Cache
sudo dnf autoremove -y
sudo dnf clean all -y
echo "$(tput setaf 4)Trash files cleaned $(tput sgr 0)"

# Enable RPM Fusion
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
echo "$(tput setaf 4)Enabled RPM Fusion Repo $(tput sgr 0)"

# Update System
sudo yum update -y
echo "$(tput setaf 4)System Update! $(tput sgr 0)"

# Install Media Codecs
sudo dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install -y lame\* --exclude=lame-devel
sudo dnf group upgrade -y --with-optional Multimedia
echo "$(tput setaf 4)Installed Multimedia Codecs $(tput sgr 0)"

# Install basic apps
sudo dnf install -y git zsh rofi flatpak input-remapper
chsh -s $(which zsh)
echo "$(tput setaf 4)Installed Basic apps $(tput sgr 0)"

# Input Remapper control
sudo systemctl enable --now input-remapper
sudo systemctl start input-remapper

# Enabling Flatpak
flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo
echo "$(tput setaf 4)Flatpak Enabled $(tput sgr 0)"

# Dotfiles Git Repo
if [ ! -d $HOME/git-reps ] ; then
	mkdir $HOME/git-reps
	echo "$(tput setaf 2)Directory created! $(tput sgr 0)"
	
else
	echo "$(tput bold)$(tput setaf 3)Directory Found! $(tput sgr 0)"
	
git clone https://gitlab.com/Spectrasonic/dotfiles.git $HOME/git-reps/dotfiles

#  Global Scripts
bash -c $HOME/git-reps/dotfiles/Scripts/bash/Fedora/brave-browser-install-fedora.sh
bash -c $HOME/git-reps/dotfiles/Scripts/bash/Global/neovim-install.sh
bash -c $HOME/git-reps/dotfiles/Scripts/bash/Global/helix_install.sh
bash -c $HOME/git-reps/dotfiles/Scripts/bash/Fedora/vscode-fedora-install.sh
#bash -c $HOME/git-reps/dotfiles/Scripts/bash/Fedora/input-remapper-install-linux.sh
#bash -c $HOME/git-reps/dotfiles/Scripts/bash/Fedora/edge_install_fedora.sh

echo
echo "$(tput bold)$(tput setaf 2)The System is up to date! Reboot to aplly Changes $(tput sgr 0)"
