#!/bin/sh

# Download Discord

wget -O Discord.tar.gz https://discord.com/api/download?platform=linux&format=tar.gz

tar -xvf Discord.tar.gz
rm -rfv Discord.tar.gz

mv ./Discord ~/opt