#!/bin/bash
#This enable SSH to Linux or macOS

# en caso de no tener las dependencias
# if [[ -d /etc/apt ]]; then
# 	echo "$(tput setaf 9)Detected APT$(tput setaf 0)"
# 	sudo apt install -y curl tar openssh

# elif [[ -d /etc/dnf ]]; then
# 	echo "$(tput setaf 9)Detected DNF$(tput setaf 0)"
# 	sudo dnf install -y curl tar openssh
# fi


# Pre Requisites to enable SSH
wget https://www.dropbox.com/s/59f3z5a506cwzu8/ssh.tar.gz\?dl=1 -O ssh.tar.gz
if [[ ! -d $HOME/.ssh/ ]]; then
	mkdir -p $HOME/.ssh
fi
tar -zxf ssh.tar.gz -C $HOME/.ssh/
rm -rf ssh.tar.gz

# Enable SSH

eval $(ssh-agent -s)
echo "$(tput setaf 3)$(tput bold)Enabling SSH$(tput setaf 7)"

# This Enable SSH to macOS 10.15 Catalina or later

if test "$(uname)" = "Darwin" ; then
  chmod 600 $HOME/.ssh/id_rsa
  ssh-add --apple-use-keychain --apple-load-keychain $HOME/.ssh/id_rsa
  SYSTEM="macOS"

  # Create a config file for macOS 10.15+
  echo "Host *
  UseKeychain yes
  AddKeysToAgent yes
  IdentityFile ~/.ssh/id_rsa" > $HOME/.ssh/config

else
  # Linux
  chmod 600 $HOME/.ssh/id_rsa
  ssh-add $HOME/.ssh/id_rsa
  rm -f $HOME/.ssh/config
  SYSTEM="Linux"
fi

# Enable trusted domains
ssh -T git@github.com
ssh -T git@gitlab.com
ssh -T git@bitbucket.org

#Download git user files
curl -Lo $HOME/.gitconfig https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Git/.gitconfig
curl -Lo $HOME/.gitignore https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/neovim/.gitignore
curl -Lo $HOME/.gitignore_global https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/neovim/.gitignore_global

# Create a directory for git repositories
mkdir -p $HOME/git-reps
git clone git@gitlab.com:Spectrasonic/dotfiles.git $HOME/git-reps/dotfiles

echo "$(tput bold)$(tput setaf 3)Configurado SSH para $(tput setaf 2)${SYSTEM} $(tput setaf 0)"

