#!/bin/sh

 # --- Tablet ---
xsetwacom --set 'HUION Huion Tablet Pad pad' Button 8 "key +ctrl +z -z -ctrl" # Sup IZQ
xsetwacom --set 'HUION Huion Tablet Pad pad' Button 2 "key e" # Inf IZQ
xsetwacom --set 'HUION Huion Tablet Pad pad' Button 1 "key b" # Sup Der
xsetwacom --set 'HUION Huion Tablet Pad pad' Button 3 "key g" # INF Der

# --- Stylus ---

xsetwacom --set 'HUION Huion Tablet stylus' Button 3 "key +ctrl +z -z -ctrl" # Arriba
xsetwacom --set 'HUION Huion Tablet stylus' Button 2 "key +space -space" # Abajo

echo "$(tput setaf 2) Huion Tablet Keys Set $(tput setaf 0)"
