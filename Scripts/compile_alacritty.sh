#!/bin/sh

# Download and compile alacritty terminal
cd ~/git-reps

# install cargo
curl https://sh.rustup.rs -sSf | sh

# This conditioal check the distro to download the correct dependences 

if test "$(egrep '^(ID)' /etc/os-release)" = "ID=fedora"; then
    echo "Is Fedora"
    # install dependencies¨
    dnf install -y cmake freetype-devel fontconfig-devel libxcb-devel libxkbcommon-devel g++

    echo
    echo
    echo "$(tput setaf 2)Installed dependencies for alacritty$(tput sgr0)"
    echo
    echo
        
elif test "$(egrep '^(ID)' /etc/os-release)" = "ID=ubuntu"; then
    echo "Is Ubuntu"
    # install dependencies¨
    apt-get install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3

    echo
    echo
    echo "$(tput setaf 2)Installed dependencies for alacritty$(tput sgr0)"
    echo
    echo


else
    echo "Maybe is macOS"
    brew install alacritty
fi

    # Clone repository
    git clone https://github.com/alacritty/alacritty.git
    cd alacritty

    # Build
    cargo build --release
    echo
    echo
    echo
    echo "$(tput setaf 2)Finish Built alacritty$(tput sgr0)"
    


