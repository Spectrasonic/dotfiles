import os
import subprocess

# Buscar archivos HTML y convertirlos a Markdown
for root, dirs, files in os.walk("."):
    for file in files:
        if file.lower().endswith(".html"):
            html_path = os.path.join(root, file)
            md_path = os.path.splitext(html_path)[0] + ".md"
            subprocess.run(["pandoc", html_path, "-o", md_path])

# Eliminar todos los archivos que no sean Markdown
for root, dirs, files in os.walk("."):
    for file in files:
        if not file.lower().endswith(".md"):
            file_path = os.path.join(root, file)
            os.remove(file_path)