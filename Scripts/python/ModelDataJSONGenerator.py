import os

# Define el path base
path_base = "item/custom/"

# Pregunta al usuario desde qué número de model data comenzar
try:
    start_cmd = int(input("¿Desde qué número de model data quieres comenzar? "))
except ValueError:
    print("Por favor, introduce un número entero válido.")
    exit()

cmd = start_cmd
json_files = []

# Obtiene el directorio de trabajo actual
current_directory = os.getcwd()

# Lista todos los archivos en el directorio actual
for filename in os.listdir(current_directory):
    # Verifica si el archivo termina con '.json'
    if filename.endswith('.json'):
        json_files.append(filename)

# Ordena los archivos para que la generación sea predecible
json_files.sort()

# Genera las líneas para cada archivo JSON
num_files = len(json_files)
for i, filename in enumerate(json_files):
    cmd += 1
    # Imprime la línea con el número de cmd actual y el nombre del archivo sin '.json'
    line = f'{{"predicate": {{"custom_model_data": {cmd}}},"model": "{path_base}{filename[:-5]}"}}'
    # Agrega la coma solo si no es el último archivo
    if i < num_files - 1:
        line += ','
    print(line)
