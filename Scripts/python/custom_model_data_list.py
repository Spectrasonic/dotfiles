import os

cmd = 0
# Lista todos los archivos en el directorio actual
for filename in os.listdir('.'):
    # Verifica si el archivo termina con '.json'
    if filename.endswith('.json'):
        cmd += 1
        # Imprime la línea con el número de cmd actual y el nombre del archivo sin '.json'
        print(f'{{"predicate": {{"custom_model_data": {cmd}}},"model": "item/custom/{filename[:-5]}"}},')