import os
import sys

def delete_files():
    deleted_files = []
    errors = []

    for root, _, files in os.walk('.'):
        for file in files:
            if file in [".DS_Store", "._.DS_Store"] or file.startswith("._"):
                full_path = os.path.join(root, file)
                try:
                    os.remove(full_path)
                    deleted_files.append(full_path)
                except PermissionError:
                    errors.append(f"Permission denied: {full_path}")
                except OSError as e:
                    errors.append(f"Error deleting {full_path}: {e}")

    # Print deleted files
    if deleted_files:
        for file in deleted_files:
            print(file)
    else:
        print("Nothing to Delete")

    # Print errors if any
    for error in errors:
        print(error, file=sys.stderr)

if __name__ == "__main__":
    delete_files()