import qrcode
from colorama import Fore, Back, Style
from PIL import Image

data = input('Enter Link: ')

qr = qrcode.QRCode(version=1, box_size=10, border=4)
qr.add_data(data)
qr.make(fit=True)

image = qr.make_image(fill="black", back_color="white")

image.save(Fore.GREEN + "qr_code.png")