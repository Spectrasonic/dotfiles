from PIL import Image # python3 -m pip install Pillow
from colorama import Fore, Back, Style # python3 -m pip install colorama
import os

cfolder = os.getcwd() + "/"
directory = "Compressed"
path = os.path.join(cfolder, directory)

if not os.path.exists(path):
    os.mkdir(path)
    print("")
    print(Fore.GREEN + "Directory '%s' created" %directory)
    print("")
else:
    print("")
    print(Fore.YELLOW + "Directory '%s' already exists" %directory)
    print("")

if __name__ == "__main__":
    for filename in os.listdir(cfolder):
        name, extension = os.path.splitext(cfolder + filename)

        if extension in [".jpg", ".jpeg", ".png"]:
            picture = Image.open(cfolder + filename)
            picture.save(path + "/" + filename, optimize=True, quality=20)
            print(Fore.YELLOW + "Converted " + filename)
else:
    print(Fore.RED + "Nothing to Convert")
