from PyPDF4 import PdfFileMerger # pip install PyPDF4
from colorama import Fore # pip install colorama
import os

merger = PdfFileMerger()

pdf_file = [file for file in os.listdir('.') if file.endswith(('.pdf'))]

for pdf in pdf_file:
    merger.append(pdf)
    print(Fore.YELLOW + f'Merge {pdf} to the pdf')

merger.write('merged.pdf')
merger.close()

for filename in os.listdir():
    if filename != ( 'merged.pdf') and filename.endswith('.pdf'):
        os.remove(filename)
