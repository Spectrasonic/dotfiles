import os
from PIL import Image       # python3 -m pip install -U pillow
from colorama import Fore   # python3 -m pip install -U colorama

if not os.path.exists('output'):
    os.makedirs('output')

images = [file for file in os.listdir('.') if file.endswith(('jpg', 'jpeg', 'png'))]

for image in images:
    im = Image.open(image)
    filename = os.path.splitext(image)[0]
    im.save(f'output/{filename}.pdf', 'PDF', resolution=100.0)
    print(Fore.YELLOW + f'Converted {image} to pdf')

print(Fore.GREEN + 'All images converted to pdf')
