import os
from colorama import Fore # pip3 install colorama

directory = os.getcwd()

for file in os.listdir(directory):
    if os.path.isfile(os.path.join(directory, file)):
        nuevo_nombre = file.lower().replace(' ', '_')
        if nuevo_nombre != file:
            os.rename(os.path.join(directory, file), os.path.join(directory, nuevo_nombre))
            print(Fore.YELLOW + f"file renombrado: {file} -> {nuevo_nombre}")
        else:
            print(Fore.YELLOW + f"No se requirió cambio: {file}")
print(Fore.RESET + "Proceso completado.")