import os
import sys

def generate_sound_resource(folder_path, namespace_prefix, resource_path):
    """
    Generates sound resource entries for .ogg files in the specified folder.

    :param folder_path: The path to the folder containing the .ogg files.
    :param namespace_prefix: The prefix for the namespace in the output.
    :param resource_path: The path to be used in the resource definition.
    """
    sound_entries = []

    for filename in os.listdir(folder_path):
        if filename.endswith(".ogg"):
            # Remove the file extension to get the base name
            base_name = filename[:-4]
            # Construct the namespace and resource path
            namespace = f'"{namespace_prefix}{base_name}"'
            sound_path = f'minecraft:{resource_path}{base_name}'
            # Append the formatted resource entry to the list
            sound_entries.append(f'{namespace}: {{"category": "master", "sounds": ["{sound_path}"]}}')

    # Print all sound entries, joining them with commas
    print(",\n".join(sound_entries))

def main():
    # Default values
    folder_path = os.getcwd()
    namespace_prefix = 'final.'
    resource_path = 'lineas_finales/'

    # Check if command-line arguments are provided
    if len(sys.argv) > 1:
        folder_path = sys.argv[1]
    if len(sys.argv) > 2:
        namespace_prefix = sys.argv[2]
    if len(sys.argv) > 3:
        resource_path = sys.argv[3]

    # Validate folder path
    if not os.path.isdir(folder_path):
        print(f"Error: The specified folder path '{folder_path}' does not exist or is not a directory.")
        sys.exit(1)

    # Generate the sound resources
    generate_sound_resource(folder_path, namespace_prefix, resource_path)

if __name__ == "__main__":
    main()
