import os
import subprocess
import shutil

# === Colores ===
BLACK = "\033[30m"
RED = "\033[31m"
GREEN = "\033[32m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
MAGENTA = "\033[35m"
CYAN = "\033[36m"
WHITE = "\033[37m"
RESET = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[4m"
ITALIC = "\033[3m"
INVERT = "\033[7m"

BBLACK = "\033[40m"
BRED = "\033[41m"
BGREEN = "\033[42m"
BYELLOW = "\033[43m"
BBLUE = "\033[44m"
BMAGENTA = "\033[45m"
BCYAN = "\033[46m"
BWHITE = "\033[47m"
BRESET = "\033[0m"

# === Variables de Versiones ===
PAPERAPI_VERSION = "1.21"
ACF_VERSION = "0.5.1"
LOMBOK_VERSION = "1.18.36"
MINIMESSAGE_VERSION = "4.18.0"
JAVA_VERSION = "21"
GRADLE_SHADOW_VERSION = "9.0.0-beta4"

# === Directorios ===
BASE_DIR = "src/main"
RESOURCES_DIR = os.path.join(BASE_DIR, "resources")
UTILS_DIR = os.path.join(BASE_DIR, "java", "com", "spectrasonic", "Utils")
MAIN_PATH = os.path.join(BASE_DIR, "java", "com", "spectrasonic", "Main.java")

def main():
    import sys
    import argparse

    parser = argparse.ArgumentParser(description="Crear un proyecto de plugin.")
    parser.add_argument("project_name", nargs="?", help="Nombre del proyecto")
    args = parser.parse_args()

    if args.project_name:
        PROJECT_NAME = args.project_name
    else:
        PROJECT_NAME = input(f"{BMAGENTA}{BLACK} Plugin Project:{RESET} ")

    if os.path.exists(PROJECT_NAME):
        print(f"{RED}El proyecto ya existe, {WHITE}Usa otro nombre!")
        sys.exit(1)

    subprocess.run(["git", "clone", "git@github.com:spectrasonic117/mkplugin.git", PROJECT_NAME], check=True, stdout=subprocess.DEVNULL)
    os.chdir(PROJECT_NAME)

    if os.path.exists(".git"):
        shutil.rmtree(".git")

    subprocess.run(["git", "init"], check=True, stdout=subprocess.DEVNULL)

    build_gradle_content = f'''plugins {{
    id "com.gradleup.shadow" version "{GRADLE_SHADOW_VERSION}"
    id "java"
}}

group = "com.spectrasonic"
version = "1.0.0"

repositories {{
    mavenCentral()
    maven {{
        name = "papermc-repo"
        url = "https://repo.papermc.io/repository/maven-public/"
    }}
    maven {{
        name = "sonatype"
        url = "https://oss.sonatype.org/content/groups/public/"
    }}
    maven {{
        name = "aikar"
        url = "https://repo.aikar.co/content/groups/aikar/"
    }}
}}

dependencies {{
    compileOnly("io.papermc.paper:paper-api:{PAPERAPI_VERSION}-R0.1-SNAPSHOT") // Paper

    // ACF Aikar
    implementation "co.aikar:acf-paper:{ACF_VERSION}-SNAPSHOT"

    // Lombok
    compileOnly "org.projectlombok:lombok:{LOMBOK_VERSION}"

    // Minimessage - Adventure
    implementation "net.kyori:adventure-text-minimessage:{MINIMESSAGE_VERSION}"
    implementation "net.kyori:adventure-api:{MINIMESSAGE_VERSION}"
    // implementation "net.kyori:adventure-text-serializer-legacy:{MINIMESSAGE_VERSION}" // Legacy
}}

shadowJar {{
    relocate "co.aikar.commands", "com.spectrasonic.{PROJECT_NAME}.acf"
    relocate "co.aikar.locales", "com.spectrasonic.{PROJECT_NAME}.locales"
}}

build.dependsOn shadowJar

def targetJavaVersion = '{JAVA_VERSION}'
java {{
    def javaVersion = JavaVersion.toVersion(targetJavaVersion)
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
    if (JavaVersion.current() < javaVersion) {{
        toolchain.languageVersion = JavaLanguageVersion.of(targetJavaVersion)
    }}
}}

tasks.withType(JavaCompile).configureEach {{
    options.encoding = "UTF-8"

    if (targetJavaVersion >= 10 || JavaVersion.current().isJava10Compatible()) {{
        options.release.set(targetJavaVersion)
    }}
}}

processResources {{
    def props = [version: version]
    inputs.properties props
    filteringCharset "UTF-8"
    filesMatching("plugin.yml") {{
        expand props
    }}
}}
'''

    with open("build.gradle", "w") as f:
        f.write(build_gradle_content)

    with open("settings.gradle", "w") as f:
        f.write(f"rootProject.name = '{PROJECT_NAME}'")

    os.makedirs(RESOURCES_DIR, exist_ok=True)
    os.makedirs(UTILS_DIR, exist_ok=True)

    plugin_yml_content = f'''name: {PROJECT_NAME}
version: '${{version}}'
main: com.spectrasonic.{PROJECT_NAME}.Main
api-version: '{PAPERAPI_VERSION}'
authors: [Spectrasonic]
'''
    with open(os.path.join(RESOURCES_DIR, "plugin.yml"), "w") as f:
        f.write(plugin_yml_content)

    main_java_content = f'''package com.spectrasonic.{PROJECT_NAME};

import com.spectrasonic.{PROJECT_NAME}.Utils.MessageUtils;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {{

    @Override
    public void onEnable() {{
        registerCommands()
        registerEvents()
        MessageUtils.sendStartupMessage(this);
    }}

    @Override
    public void onDisable() {{
        MessageUtils.sendShutdownMessage(this);
    }}

    public void registerCommands() {{
        // Set Commands Here
    }}

    public void registerEvents() {{
        // Set Events Here
    }}
}}
'''
    with open(os.path.join(MAIN_PATH, "Main.java"), "w") as f:
        f.write(main_java_content)

    message_utils_java_content = f'''package com.spectrasonic.{PROJECT_NAME}.Utils;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class MessageUtils {{

    public static final String DIVIDER = "<gray>----------------------------------------</gray>";
    public static final String PREFIX = "<gray>[<gold>{PROJECT_NAME}</gold>]</gray> <gold>»</gold>";

    private static final MiniMessage miniMessage = MiniMessage.miniMessage();

    private MessageUtils() {{
        // Private constructor to prevent instantiation
    }}

    public static void sendMessage(CommandSender sender, String message) {{
        sender.sendMessage(miniMessage.deserialize(PREFIX + message));
    }}

    public static void sendConsoleMessage(String message) {{
        Bukkit.getConsoleSender().sendMessage(miniMessage.deserialize(PREFIX + message));
    }}

    public static void sendPermissionMessage(CommandSender sender) {{
        sender.sendMessage(miniMessage.deserialize(PREFIX + "<red>You do not have permission to use this command!</red>"));
    }}

    public static void sendStartupMessage(JavaPlugin plugin) {{
        String[] messages = {{
                DIVIDER,
                PREFIX + "<white>" + plugin.getDescription().getName() + "</white> <green>Plugin Enabled!</green>",
                PREFIX + "<light_purple>Version: </light_purple>" + plugin.getDescription().getVersion(),
                PREFIX + "<white>Developed by: </white><red>" + plugin.getDescription().getAuthors() + "</red>",
                DIVIDER
        }};

        for (String message : messages) {{
            Bukkit.getConsoleSender().sendMessage(miniMessage.deserialize(message));
        }}
    }}

    public static void sendVeiMessage(JavaPlugin plugin) {{
        String[] messages = {{
                PREFIX + "⣇⣿⠘⣿⣿⣿⡿⡿⣟⣟⢟⢟⢝⠵⡝⣿⡿⢂⣼⣿⣷⣌⠩⡫⡻⣝⠹⢿⣿⣷",
                PREFIX + "⡆⣿⣆⠱⣝⡵⣝⢅⠙⣿⢕⢕⢕⢕⢝⣥⢒⠅⣿⣿⣿⡿⣳⣌⠪⡪⣡⢑⢝⣇",
                PREFIX + "⡆⣿⣿⣦⠹⣳⣳⣕⢅⠈⢗⢕⢕⢕⢕⢕⢈⢆⠟⠋⠉⠁⠉⠉⠁⠈⠼⢐⢕⢽",
                PREFIX + "⡗⢰⣶⣶⣦⣝⢝⢕⢕⠅⡆⢕⢕⢕⢕⢕⣴⠏⣠⡶⠛⡉⡉⡛⢶⣦⡀⠐⣕⢕",
                PREFIX + "⡝⡄⢻⢟⣿⣿⣷⣕⣕⣅⣿⣔⣕⣵⣵⣿⣿⢠⣿⢠⣮⡈⣌⠨⠅⠹⣷⡀⢱⢕",
                PREFIX + "⡝⡵⠟⠈⢀⣀⣀⡀⠉⢿⣿⣿⣿⣿⣿⣿⣿⣼⣿⢈⡋⠴⢿⡟⣡⡇⣿⡇⡀⢕",
                PREFIX + "⡝⠁⣠⣾⠟⡉⡉⡉⠻⣦⣻⣿⣿⣿⣿⣿⣿⣿⣿⣧⠸⣿⣦⣥⣿⡇⡿⣰⢗⢄",
                PREFIX + "⠁⢰⣿⡏⣴⣌⠈⣌⠡⠈⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣬⣉⣉⣁⣄⢖⢕⢕⢕",
                PREFIX + "⡀⢻⣿⡇⢙⠁⠴⢿⡟⣡⡆⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣵⣵⣿",
                PREFIX + "⡻⣄⣻⣿⣌⠘⢿⣷⣥⣿⠇⣿⣿⣿⣿⣿⣿⠛⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿",
                PREFIX + "⣷⢄⠻⣿⣟⠿⠦⠍⠉⣡⣾⣿⣿⣿⣿⣿⣿⢸⣿⣦⠙⣿⣿⣿⣿⣿⣿⣿⣿⠟",
                PREFIX + "⡕⡑⣑⣈⣻⢗⢟⢞⢝⣻⣿⣿⣿⣿⣿⣿⣿⠸⣿⠿⠃⣿⣿⣿⣿⣿⣿⡿⠁⣠",
                PREFIX + "⡝⡵⡈⢟⢕⢕⢕⢕⣵⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣶⣿⣿⣿⣿⣿⠿⠋⣀⣈⠙",
                PREFIX + "⡝⡵⡕⡀⠑⠳⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⢉⡠⡲⡫⡪⡪⡣",
        }};

        for (String message : messages) {{
            Bukkit.getConsoleSender().sendMessage(miniMessage.deserialize(message));
        }}
    }}

    public static void sendBroadcastMessage(String message) {{
        for (Player player : Bukkit.getOnlinePlayers()) {{
            player.sendMessage(miniMessage.deserialize(message));
        }}
    }}

    public static void sendShutdownMessage(JavaPlugin plugin) {{
        String[] messages = {{
                DIVIDER,
                PREFIX + "<red>" + plugin.getDescription().getName() + " plugin Disabled!</red>",
                DIVIDER
        }};

        for (String message : messages) {{
            Bukkit.getConsoleSender().sendMessage(miniMessage.deserialize(message));
        }}
    }}
}}
'''
    with open(os.path.join(UTILS_DIR, "MessageUtils.java"), "w") as f:
        f.write(message_utils_java_content)

    sound_utils_java_content = f'''package com.spectrasonic.{PROJECT_NAME}.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public final class SoundUtils {{

    private SoundUtils() {{
        // Private constructor to prevent instantiation
    }}

    public static void playerSound(Player player, Sound sound, float volume, float pitch) {{
        player.playSound(player, sound, SoundCategory.MASTER, volume, pitch);
    }}

    public static void broadcastPlayerSound(Sound sound, float volume, float pitch) {{
        for (Player player : Bukkit.getOnlinePlayers()) {{
            player.playSound(player, sound, SoundCategory.MASTER, volume, pitch);
        }}
    }}
}}
'''
    with open(os.path.join(UTILS_DIR, "SoundUtils.java"), "w") as f:
        f.write(sound_utils_java_content)

    os.system("clear")
    print(f"Project: {MAGENTA}{PROJECT_NAME}{RESET} created successfully.")
    print(f"Compiler: {CYAN}Gradle{RESET}")
    print(f"Paper: {MAGENTA}{PAPERAPI_VERSION}{RESET}")

if __name__ == "__main__":
    main()
