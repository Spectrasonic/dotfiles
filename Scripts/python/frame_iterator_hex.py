import os
import yaml

def get_user_input():
    """
    Prompts the user for input and returns the necessary parameters.
    """
    filename = input("Enter the base filename: ").strip()
    dir_path = input("Enter the directory path: ").strip()
    num_iterations = int(input("Enter the number of iterations: ").strip())
    start_frame = int(input("Enter the starting frame number (0-4095): ").strip())

    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
        print(f"Directory {dir_path} created.")

    if not (0 <= start_frame <= 4095):
        raise ValueError("Starting frame number must be between 0 and 4095.")

    return filename, dir_path, num_iterations, start_frame

def generate_yaml_content(filename, dir_path, start_frame, num_iterations):
    """
    Generates the YAML content for all iterations in numerical order.
    """
    content = {}
    for i in range(num_iterations):
        current_frame = start_frame + i
        if current_frame > 4095:
            print("Maximum frame number (4095) reached. Stopping iterations.")
            break

        frame_hex = f"\\uE{current_frame:03X}"
        content[f"{filename}_{i + 1}"] = {
            "texture": f"{dir_path}/{filename}_{i + 1}",
            "ascent": 30,
            "height": 64,
            "char": frame_hex
        }
    return content

def save_yaml_file(content, filename, dir_path):
    """
    Saves the YAML content to a single file.
    """
    file_path = os.path.join(dir_path, f"{filename}.yaml")
    with open(file_path, 'w') as file:
        yaml.dump(content, file, default_flow_style=False, sort_keys=False)
    print(f"File saved: {file_path}")

def main():
    """
    Main function to execute the script.
    """
    filename, dir_path, num_iterations, start_frame = get_user_input()
    content = generate_yaml_content(filename, dir_path, start_frame, num_iterations)
    save_yaml_file(content, filename, dir_path)

if __name__ == "__main__":
    main()
