import os
from colorama import Fore
import string

def clean_name(name):
    # Separar el nombre y la extensión si es un archivo
    name_part, ext = os.path.splitext(name) if '.' in name else (name, '')
    
    # Convertir a minúsculas
    name_part = name_part.lower()
    
    # Reemplazar espacios y guiones medios por guiones bajos
    name_part = name_part.replace(' ', '_').replace('-', '_')
    
    # Eliminar caracteres especiales (puntos, comas, paréntesis, etc.)
    valid_chars = string.ascii_lowercase + string.digits + '_'  # Removido el guión medio
    name_part = ''.join(c for c in name_part if c in valid_chars)
    
    # Eliminar guiones bajos múltiples consecutivos
    while '__' in name_part:
        name_part = name_part.replace('__', '_')
    
    # Eliminar guiones bajos al inicio o final
    name_part = name_part.strip('_')
    
    # Si tenemos extensión, la agregamos de nuevo (manteniendo su caso original)
    if ext:
        return name_part + ext
    return name_part

def rename_items(directory):
    # Primero renombrar los archivos
    for root, dirs, files in os.walk(directory, topdown=False):
        # Renombrar archivos
        for file in files:
            old_path = os.path.join(root, file)
            new_name = clean_name(file)
            
            if new_name != file:
                new_path = os.path.join(root, new_name)
                try:
                    os.rename(old_path, new_path)
                    print(Fore.GREEN + f"Archivo renombrado: {file} -> {new_name}")
                except OSError as e:
                    print(Fore.RED + f"Error al renombrar {file}: {e}")
            else:
                print(Fore.YELLOW + f"No se requirió cambio en archivo: {file}")
        
        # Renombrar directorios
        for dir_name in dirs:
            old_path = os.path.join(root, dir_name)
            new_name = clean_name(dir_name)
            
            if new_name != dir_name:
                new_path = os.path.join(root, new_name)
                try:
                    os.rename(old_path, new_path)
                    print(Fore.GREEN + f"Carpeta renombrada: {dir_name} -> {new_name}")
                except OSError as e:
                    print(Fore.RED + f"Error al renombrar carpeta {dir_name}: {e}")
            else:
                print(Fore.YELLOW + f"No se requirió cambio en carpeta: {dir_name}")

def main():
    try:
        directory = os.getcwd()
        print(Fore.CYAN + f"Iniciando proceso en: {directory}")
        rename_items(directory)
        print(Fore.RESET + "Proceso completado exitosamente.")
    except Exception as e:
        print(Fore.RED + f"Error general: {e}")
    finally:
        print(Fore.RESET)

if __name__ == "__main__":