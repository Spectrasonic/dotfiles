import csv

with open('items.csv', mode='r') as csv_file:
    csv_reader = csv.reader(csv_file)
    
    with open('output.txt', mode='w') as txt_file:
        for row in csv_reader:
            col1 = row[0]
            col2 = row[1]
            
            print(f'{col1}({col1}, {col2}, World.Environment.NETHER),')
            txt_file.write(f'{col1}({col1}, {col2}, World.Environment.NETHER),\n')
