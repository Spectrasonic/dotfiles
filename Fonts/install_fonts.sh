#!bin/bash

# == Install Fonts in any Linux distros ==

# Create folder if not exist

if [ ! -d /usr/local/share/fonts ]; then
   sudo mkdir -p /usr/local/share/fonts
else
  echo ""
  echo "Directory Found"
fi

# Copy all files to Fonts Directory
sudo cp ./*.ttf /usr/local/share/fonts
# sudo cp ./*.otf /usr/local/share/fonts

# Load the Fots in System
fc-cache -f

echo "$(tput setaf 2)All Fonts Installed. $(tput sgr 0)"

exit 1
