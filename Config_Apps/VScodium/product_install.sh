#!/bin/bash

echo "$(tput setaf 3)============================= $(tput sgr 0)"
echo "$(tput setaf 3) Select Version of VSCodium $(tput sgr 0)"
echo "$(tput setaf 3)============================= $(tput sgr 0)"
echo 
echo "$(tput setaf 3)1) Flatpak $(tput sgr 0)"
echo "$(tput setaf 3)2) Others $(tput sgr 0)"
# echo "$(tput setaf 3)3) Snap $(tput sgr 0)"
echo 
echo "$(tput setaf 3)============================ $(tput sgr 0)"

read choose
case $choose in
    1)
        if [ ! -d $HOME/.var/app/com.vscodium.codium/config/VSCodium/ ]; then
            echo "$(tput setaf 3)Dir not found, Creating... $(tput sgr 0)"
            mkdir -p $HOME/.var/app/com.vscodium.codium/config/VSCodium/
        else
        	echo "$(tput setaf 3)Dir found, Copying $(tput sgr 0)"
        fi
        
        curl -Lo $HOME/.var/app/com.vscodium.codium/config/VSCodium/product.json https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config_Apps/VScodium/product.json
        echo "$(tput setaf 2)Installed product.json for Flatpak $(tput sgr 0)"
        ;;

    2)
        if [ ! -d $HOME/.config/VSCodium/ ]; then
            echo "$(tput setaf 3)Dir not found, Creating... $(tput sgr 0)"
            mkdir -p $HOME/.config/VSCodium/
        else
        	echo "$(tput setaf 3)Dir found, Copying $(tput sgr 0)"
        fi
        
        echo "$(tput setaf 3)Dir found, Copying $(tput sgr 0)"
        curl -Lo $HOME/.config/VSCodium/product.json https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config_Apps/VScodium/product.json
		echo "$(tput setaf 2)Installed product.json for Others $(tput sgr 0)"
        ;;

    # 3)
    #  echo "$(tput setaf 1)This option not aviliable yet $(tput sgr 0)"
    #  echo "$(tput setaf 1)Re run this script $(tput sgr 0)"
    #  exit 0
    #  ;;

    exit | quit | q)
        exit 0
        ;;

    *)
        echo "$(tput setaf 1)============================ $(tput sgr 0)"
        echo "$(tput setaf 1) Invalid Option re-runing Script $(tput sgr 0)"
        echo "$(tput setaf 1)============================ $(tput sgr 0)"
        sleep 1
        bash $PWD/product_install.sh
        # exit 0
        ;;
esac