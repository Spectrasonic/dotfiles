#!/bin/sh

REPO=~/git-reps/dotfiles
echo "$(tput setaf 3)Downloading Confgis from repo $(tput sgr 0)"

# --- ZSH Configuation ---
#cp -r ${REPO}/config/ZSH/* $HOME

# --- NVIM Configuation ---
#cp -r ${REPO}/config/neovim/.vimrc $HOME
#cp -r ${REPO}/config/neovim/nvim $HOME/.config

# --- GIT Configuation ---
curl -Lo $HOME/.gitconfig https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Git/.gitconfig

curl -Lo $HOME/.gitignore https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Git/.gitignore

curl -Lo $HOME/.gitignore_global https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Git/.gitignore_global

# --- Terminals ---

# Alacritty
curl -Lo $HOME/.alacritty.yml https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Alacritty/.alacritty.yml

# Hyper
cp -r ${REPO}/config/Hyper/.hyper.js $HOME/

curl -Lo $HOME/.hyper.js https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Hyper/.hyper.js

#Kitty
if [ ! -d $HOME/.config/kitty/ ]; then
    mkdir -p $HOME/.config/kitty/
fi
curl -Lo $HOME/.config/kitty/kitty.conf https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/kitty/kitty.conf

#Tmux
curl -Lo $HOME/.tmux.conf https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Tmux/.tmux.conf

# --- Rofi Configuration ---

# Rofi Theme
if [ ! -d $HOME/.config/rofi/ ]; then
    mkdir -p $HOME/.config/rofi/
fi
curl -Lo $HOME/.config/rofi/config.rasi https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Rofi/config.rasi

# --- Scripts ---
#cp -r ~/git-reps/dotfiles/Scripts $HOME

# --- Fonts ---
#sudo cp -r ~/git-reps/dotfiles/Fonts/* /usr/share/fonts && sudo fc-cache -fv

echo
echo "$(tput setaf 2)$(tput bold)Done! $(tput sgr 0)"