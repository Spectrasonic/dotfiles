#!/bin/bash

# install configuration for neofetch

mkdir -p $HOME/.config/neofetch
cp ./config.conf $HOME/.config/neofetch/config.conf
cp ./vei.png $HOME/.config/neofetch/vei.png
cp ./logo $HOME/.config/neofetch/logo

command neofetch

echo "$(tput setaf 2)Copied Neofetch configuration $(tput sgr 0)"
neofetch