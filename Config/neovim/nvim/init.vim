" ██╗   ██╗██╗███╗   ███╗   ██╗███╗   ██╗██╗████████╗
" ██║   ██║██║████╗ ████║   ██║████╗  ██║██║╚══██╔══╝
" ██║   ██║██║██╔████╔██║   ██║██╔██╗ ██║██║   ██║   
" ╚██╗ ██╔╝██║██║╚██╔╝██║   ██║██║╚██╗██║██║   ██║   
"  ╚████╔╝ ██║██║ ╚═╝ ██║██╗██║██║ ╚████║██║   ██║   
"   ╚═══╝  ╚═╝╚═╝     ╚═╝╚═╝╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   
"                                                    


set runtimepath^=~/.vim runtimepath+=~/.vim/after
    let &packpath = &runtimepath
" source ~/.vimrc
set t_Co=256
syntax on

if has ("autocmd")
	au BufReadPost e if line "'\"" > && 1 && line ("'\"") <= line ("$") | exe  "normal! g'
endif

set clipboard=unnamedplus
set background=dark
set encoding=utf-8
set rnu nu
set mouse=a
set shiftwidth=2
set tabstop=4
set expandtab
set autoindent
filetype indent on


nmap <c-s> :w<cr>
imap <c-s> <esc>:w<cr>a
" end of old configuration

"let g:airline_theme='onehalfdarkv'


call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-surround'
Plug 'Yggdroot/indentLine',
Plug 'mattn/emmet-vim',
Plug 'scrooloose/nerdtree',
Plug 'deltawhy/vim-mcfunction',
Plug 'christoomey/vim-tmux-navigator',
Plug 'ryanoasis/vim-devicons',
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'joshdick/onedark.vim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" Plug 'chun-yang/auto-pairs',
" Plug 'itchyny/lightline.vim',
" Plug 'dracula/vim', {'as': 'dracula'}
" Plug 'sonph/onehalf', { 'rpt': 'vim' }
call plug#end()

" Themes
" onedark, one, dracula
"colorscheme one
 
 set noshowmode
 "let g:Powerline_symbols = 'fancy'
 "set fillchars+=stl:\ ,stlnc:\

" Tree Configuration
map <Space> <Leader>
let mapleader=" "
 nnoremap <leader>n :NERDTreeFocus<CR>
 nnoremap <C-n> :NERDTree<CR>
 nnoremap <leader>e :NERDTreeToggle<CR>
 nnoremap <leader>f :NERDTreeFind<CR>




 " Airline Config
let g:airline_theme='powerlineish'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'


" Telescope - File Finder

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

" Using Lua functions
nnoremap <leader>ff <cmd>lua require('telescope.builtin').find_files()<cr>
nnoremap <leader>fg <cmd>lua require('telescope.builtin').live_grep()<cr>
nnoremap <leader>fb <cmd>lua require('telescope.builtin').buffers()<cr>
nnoremap <leader>fh <cmd>lua require('telescope.builtin').help_tags()<cr>