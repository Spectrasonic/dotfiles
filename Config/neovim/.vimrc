" ____  ____  _____ ____ _____ ____      _    
"/ ___||  _ \| ____/ ___|_   _|  _ \    / \   
"\___ \| |_) |  _|| |     | | | |_) |  / _ \  
" ___) |  __/| |__| |___  | | |  _ <  / ___ \ 
"|____/|_|   |_____\____| |_| |_| \_\/_/   \_\
"__     _____ __  __ 
"\ \   / /_ _|  \/  |
" \ \ / / | || |\/| |
"  \ V /  | || |  | |
"   \_/  |___|_|  |_|
                    


" ██╗   ██╗██╗███╗   ███╗██████╗  ██████╗
" ██║   ██║██║████╗ ████║██╔══██╗██╔════╝
" ██║   ██║██║██╔████╔██║██████╔╝██║     
" ╚██╗ ██╔╝██║██║╚██╔╝██║██╔══██╗██║     
"  ╚████╔╝ ██║██║ ╚═╝ ██║██║  ██║╚██████╗
"   ╚═══╝  ╚═╝╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════╝
"                                        

syntax on

if has ("autocmd")
	au BufReadPost e if line "'\"" > && 1 && line ("'\"") <= line ("$") | exe  "normal! g'
endif

set clipboard=unnamedplus
set background=dark
set encoding=utf-8
set rnu nu
set mouse=a
set shiftwidth=2
set tabstop=4
set expandtab
set autoindent
filetype indent on
