#!/usr/bin/env bash

cp ./.alacritty.toml $HOME/.alacritty.toml
cp ./alacritty/ $HOME/.config/alacritty/ -r

echo " Alacritty dots installed successfully"