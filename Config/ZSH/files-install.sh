#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# Install Powelevel10K Theme
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Reintalar Plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Borrar generados
rm $HOME/.zshrc
rm $HOME/.p10k.zsh

# Descargar archivos locales
curl -Lo $HOME/.zshrc https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/.zshrc
curl -Lo $HOME/.p10k.zsh https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/.p10k.zsh

curl -Lo $HOME/.aliases https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/.aliases
curl -Lo $HOME/.enviroment https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/.enviroment
curl -Lo $HOME/.functions https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/.functions

#Reload zsh
source ~/.zshrc

echo "$(tput setaf 11)Oh My ZSH Reinstalado"
echo 
echo "$(tput setaf 2)Done!"