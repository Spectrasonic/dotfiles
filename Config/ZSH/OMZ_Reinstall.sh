#!/usr/bin/env bash
# -*- coding: utf-8 -*-

#Script to reinstall all ZSH, OhMyZSH and Powerlevel10K theme

# Instalar zshrc
# sudo apt install -y zsh

# Remove prev files

rm $HOME/.zshrc
rm $HOME/.p10k.zsh
rm -rf ~/.oh-my-zsh

# install Oh-My-ZSH
command bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"