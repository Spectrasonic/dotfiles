# Bash Aliases

alias cl="clear"
alias cd="cd $HOME"
alias ..="cd .."
alias reps="cd $HOME/git-reps"

alias l="ls -lh1"
alias l="la -lah1"
alias c="cat"


alias ga="git add -A"
alias gp="git push -u origin master"
alias pu="git pull"
alias gst="git status"
alias gc="git clone"

alias nv="vim"