#!/bin/bash

echo "$(tput bold)$(tput setaf 2)Downloading config files...$(tput sgr0)"

curl -Lo $HOME/.gitconfig https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/Git/.gitconfig
curl -Lo $HOME/.gitignore https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/neovim/.gitignore
curl -Lo $HOME/.gitignore_global https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/neovim/.gitignore_global

echo "$(tput bold)$(tput setaf 2)Done! $(tput sgr0)"
