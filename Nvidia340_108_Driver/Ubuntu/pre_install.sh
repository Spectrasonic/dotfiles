#!/bin/sh

# Preinstall Nvidia Driver 340.108 for Debian / Ubuntu Ditributions

# sudo apt-get install -y wget patch

if [ ! -d $HOME/NVIDIA ]; then
    mkdir $HOME/NVIDIA
fi

cd $HOME/NVIDIA

wget "https://www.dropbox.com/s/aujsoz1txt3r26m/NVIDIA-Linux-x86_64-340.108.run\?dl\=1"
wget -O inttf-nvidia-patcher.sh https://nvidia.if-not-true-then-false.com/patcher/inttf-nvidia-patcher.sh
chmod +x $PWD/inttf-nvidia-patcher.sh
bash $PWD/inttf-nvidia-patcher.sh -v 340.108

exit 1
