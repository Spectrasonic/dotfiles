#!/bin/bash

sudo su

## Ubuntu / Debian / Linux Mint / LMDE ##
apt update
apt upgrade
## Debian and Linux Mint ##
apt autoremove $(dpkg -l nvidia-driver* |grep ii |awk '{print $2}')
## Ubuntu ##
apt autoremove $(dpkg -l xserver-xorg-video-nvidia* |grep ii |awk '{print $2}')
apt reinstall xserver-xorg-video-nouveau

echo "$(tput setaf 1)Reiniciar Sistema $(tput sgr 0)"
