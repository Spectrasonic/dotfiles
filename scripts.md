# Scripts

# Fedora

**Fedora Post Install Script**

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Scripts/bash/Fedora/post_install.sh)"
```

---

# Global

**Install ZSH**

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/OMZ_Reinstall.sh)"
```

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Config/ZSH/zsh-files-install.sh)"
```

**Install Kitty Terminal**

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Scripts/bash/Global/kitty_install.sh)"
```

**Install Neovim**

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Scripts/bash/Global/neovim-install.sh)"
```

**BAT and LSD**

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Scripts/bash/Global/lsd_bat_install.sh)"
```

```bash
bash -c "$(curl -fsSL https://gitlab.com/Spectrasonic/dotfiles/-/raw/master/Scripts/configure_ssh.sh)"
```

---

# Ubuntu / Debian
